#!/bin/bash

sudo yum update -y
sudo yum install amazon-linux-extras-install -y docker
sudo systemctl enable docker.service
sudo systemctl start docker.service
sudo group add docker
sudo usermod -aG docker ${USER}