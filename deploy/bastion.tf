data "aws_ami" "amazon_linux" {
  owners      = ["amazon"]
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }

}
resource "aws_iam_role" "bastion" {
  name               = "${local.prefix}-bastion"
  assume_role_policy = file("./templates/bastion/instance-profile-policy.json")

  tags = local.common_tags
}

resource "aws_iam_role_policy_attachment" "bastion_attach_policy" {
  role       = aws_iam_role.bastion.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_instance_profile" "bastion" {
  name = "${local.prefix}-bastion-instance-profile"
  role = aws_iam_role.bastion.name
}

resource "aws_instance" "bastion" {
  ami                    = data.aws_ami.amazon_linux.id
  instance_type          = "t2.micro"
  user_data              = file("./templates/bastion/user-data.sh")
  iam_instance_profile   = aws_iam_instance_profile.bastion.name
  key_name               = var.bastion_key_name
  subnet_id              = aws_subnet.public_a.id
  vpc_security_group_ids = [aws_security_group.bastion.id]
  tags = merge(
    local.common_tags,
    {
      Name = "${local.prefix}-bastion"
    }
  )
}

resource "aws_security_group" "bastion" {
  description = "Control bastion outbound and inboud access"
  name        = "${local.prefix}-bastion"
  vpc_id      = aws_vpc.main.id

  ingress = [{
    cidr_blocks      = ["0.0.0.0/0"]
    description      = "SSH Access"
    from_port        = 22
    ipv6_cidr_blocks = null
    prefix_list_ids  = null
    protocol         = "tcp"
    security_groups  = null
    self             = false
    to_port          = 22
  }]

  egress = [{
    cidr_blocks      = ["0.0.0.0/0"]
    description      = "Allow https"
    from_port        = 443
    ipv6_cidr_blocks = null
    prefix_list_ids  = null
    protocol         = "tcp"
    security_groups  = null
    self             = false
    to_port          = 443
    },

    {
      cidr_blocks      = ["0.0.0.0/0"]
      description      = "Allow http"
      from_port        = 80
      ipv6_cidr_blocks = null
      prefix_list_ids  = null
      protocol         = "tcp"
      security_groups  = null
      self             = false
      to_port          = 80
    },

    {
      cidr_blocks = [
        aws_subnet.private_a.cidr_block,
        aws_subnet.private_b.cidr_block,
      ]
      description      = "Allow https"
      from_port        = 5432
      ipv6_cidr_blocks = null
      prefix_list_ids  = null
      protocol         = "tcp"
      security_groups  = null
      self             = false
      to_port          = 5432
    }
  ]
  tags = local.common_tags

}