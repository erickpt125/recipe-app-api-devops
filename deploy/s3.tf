resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.prefix}-upload-files-1250"
  acl           = "public-read"
  force_destroy = true
}

