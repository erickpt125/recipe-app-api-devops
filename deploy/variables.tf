variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "erick.puga@hotmail.com"
}

variable "db_username" {
  description = "Username for RDS postgres instances"
}

variable "db_password" {
  description = "Password for RDS postgres instances"
}

variable "bastion_key_name" {
  default = "Erick-Puga-Mac-DOU"
}

variable "ecr_image_api" {
  description = "ECR iamge for API"
  default     = "300468668613.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR iamge for proxy"
  default     = "300468668613.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret for Django app"
}